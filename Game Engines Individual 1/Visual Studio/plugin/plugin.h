//Nathan Boldy 100653593
//Referencing tutorial code
#pragma once

#include "PluginSettings.h"

// Core Libraries (std::)
#include <iostream>
#include <fstream>
#include <string>
#include <direct.h>

#pragma region //declaring functions

class PLUGIN_API Plugin {
public:
	//Saving and loading
	void Load(std::string filePath);
	void Save(std::string filePath);

	//functions used to assign values around saving and loading
	void SetPosition(float _position[3]);
	void SetRotation(float _rotation[3]);

	//get position values
	float getX();
	float getY();
	float getZ();

	//get rotation values
	float getRotX();
	float getRotY();
	float getRotZ();

	//populates array for safety
	float position[3] = { 0.f, 0.f, 0.f };
	float rotation[3] = { 0.f, 0.f, 0.f };
};

#pragma endregion
