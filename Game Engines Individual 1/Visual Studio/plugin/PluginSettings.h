//Nathan Boldy 100653593
//Referencing tutorial code 
#pragma once
#ifdef ASSIGNMENTPLUGIN_EXPORTS
#define PLUGIN_API __declspec(dllexport)
#elif ASSIGNMENTPLUGIN_IMPORTS
#define PLUGIN_API __declspec(dllimport)
#else
#define PLUGIN_API
#endif