//Nathan Boldy 100653593
//Referencing Tutorial code
#pragma once

#include "PluginSettings.h"
#include "plugin.h"
#include <string.h>

#ifdef __cplusplus
extern "C" {
#endif
	// File IO
	PLUGIN_API void Load(char* filePath);
	PLUGIN_API void Save(char* filePath);

	//set position/rotation for saving and loading purposes
	PLUGIN_API void SetPosition(float _position[3]);
	PLUGIN_API void SetRotation(float _rotation[3]);

	// get position data
	PLUGIN_API float getX();
	PLUGIN_API float getY();
	PLUGIN_API float getZ();

	// get rotation data
	PLUGIN_API float getRotX();
	PLUGIN_API float getRotY();
	PLUGIN_API float getRotZ();

#ifdef __cplusplus
}
#endif
