//Nathan Boldy 100653593
#include "plugin.h"

#pragma region //Loading Function

void Plugin::Load(std::string filePath)
{
	//String that cotains our file path
	std::string outPath = filePath;
	//Temporary string that contains file data so we can scan it
	std::string tempStr;

	//Opens file path
	std::ifstream textFile(outPath);
	//Safety check that file is indeed open
	if (textFile.is_open()) {

		while (std::getline(textFile, tempStr))
		{
			//Safety set of float array to 0,0,0
			float pos[3] = { 0.f, 0.f, 0.f };
			//Gets position values based on file format
			std::sscanf(tempStr.c_str(), "%f %f %f", &pos[0], &pos[1], &pos[2]);

			//gets next line
			std::getline(textFile, tempStr);

			//Safety set of float array to 0,0,0
			float rot[3] = { 0.f, 0.f, 0.f };
			//Gets rotation values based on file format
			std::sscanf(tempStr.c_str(), "%f %f %f", &rot[0], &rot[1], &rot[2]);

			//Store your position
			position[0] = pos[0];
			position[1] = pos[1];
			position[2] = pos[2];

			//Store rotation
			rotation[0] = rot[0];
			rotation[1] = rot[1];
			rotation[2] = rot[2];
		}
		//closes file
		textFile.close();
	}

}
#pragma endregion

#pragma region //Save Function

void Plugin::Save(std::string filePath)
{
	//String that stores our file path
	std::string outPath = filePath;
	//Temporary string that contains file data so we can scan it
	std::string tempStr;

	//Opens file
	std::ofstream textFile(outPath);
	//Safety check that file is actually open
	if (textFile.is_open()) {
		//writes position float array to file then goes to next line
		textFile << position[0] << " " << position[1] << " " << position[2] << std::endl;
		//writes rotation float array to file then goes to next line
		textFile << rotation[0] << " " << rotation[1] << " " << rotation[2] << std::endl;
	}
	//closes file
	textFile.close();
}
#pragma endregion

#pragma region //Setters

//Sets position values
void Plugin::SetPosition(float _position[3])
{
	position[0] = _position[0];
	position[1] = _position[1];
	position[2] = _position[2];
}

//sets rotation values
void Plugin::SetRotation(float _rotation[3])
{
	rotation[0] = _rotation[0];
	rotation[1] = _rotation[1];
	rotation[2] = _rotation[2];
}

#pragma endregion

#pragma region //Getters

float Plugin::getX()
{
	//Returns "X coord" of position float array
	return position[0];
}

float Plugin::getY()
{
	//Returns "Y coord" of position float array
	return position[1];
}

float Plugin::getZ()
{
	//Returns "Z coord" of position float array
	return position[2];
}

float Plugin::getRotX()
{
	//Returns "X coord" of rotation float array
	return rotation[0];
}

float Plugin::getRotY()
{
	//Returns "Y coord" of rotation float array
	return rotation[1];
}

float Plugin::getRotZ()
{
	//Returns "Z coord" of rotation float array
	return rotation[2];
}

#pragma endregion
