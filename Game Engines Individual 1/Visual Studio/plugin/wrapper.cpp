//Nathan Boldy 100653593
//referencing tutorial code
#include "Wrapper.h"

Plugin commands;

#pragma region //Setting up plugin commands

PLUGIN_API void Load(char* filePath)
{
	return commands.Load(std::string(filePath));
}

PLUGIN_API void Save(char* filePath)
{
	return commands.Save(std::string(filePath));
}

PLUGIN_API void SetPosition(float _position[3])
{
	return commands.SetPosition(_position);
}

PLUGIN_API void SetRotation(float _rotation[3])
{
	return commands.SetRotation(_rotation);
}

PLUGIN_API float getX()
{
	return commands.getX();
}

PLUGIN_API float getY()
{
	return commands.getY();
}

PLUGIN_API float getZ()
{
	return commands.getZ();
}

PLUGIN_API float getRotX()
{
	return commands.getRotX();
}

PLUGIN_API float getRotY()
{
	return commands.getRotY();
}

PLUGIN_API float getRotZ()
{
	return commands.getRotZ();
}
#pragma endregion


