﻿//Nathan Boldy 100653593
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class basicMove : MonoBehaviour
{
	//Add component field
	[SerializeField]
	GameObject player;

	//Super basic player controller
	void FixedUpdate()
    {
		//check keypress 
		if (Input.GetKey(KeyCode.W))
		{
			//move player to it's current position + 0.1 on the Z axis every frame
			player.transform.position = (player.transform.position + new Vector3(0, 0, 0.1f));
		}
		if (Input.GetKey(KeyCode.S))
		{
			//move player to it's current position + -0.1 on the Z axis every frame
			player.transform.position = (player.transform.position + new Vector3(0, 0, -0.1f));
		}
		if (Input.GetKey(KeyCode.A))
		{
			//move player to it's current position -0.1 on the X axis every frame
			player.transform.position = (player.transform.position + new Vector3(-0.1f, 0, 0));
		}
		if (Input.GetKey(KeyCode.D))
		{
			//move player to it's current position + 0.1 on the X axis every frame
			player.transform.position = (player.transform.position + new Vector3(0.1f, 0, 0));
		}
	}
}
