﻿//Nathan Boldy 100653593

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.InteropServices;

public class pluginTest : MonoBehaviour
{
	#region //Misc Definitions

	//Name of plugin for import purposes
	const string DLL_NAME = "PLUGIN";

	//Location of file that contains our save/load data
	string filePath = "./ Assets / Test.txt";

	//Defining our component fields for the objects we save data from, and spawn from loaded data
	[SerializeField]
	GameObject cube;
	[SerializeField]
	GameObject LoadableCube;
	#endregion

	#region //Import plugin functions
	[DllImport(DLL_NAME)]
	private static extern void Load([MarshalAs(UnmanagedType.LPStr)]string filePath);
	[DllImport(DLL_NAME)]
	private static extern void Save([MarshalAs(UnmanagedType.LPStr)]string filePath);
	#endregion

	#region //Import plugin Setters
	[DllImport(DLL_NAME)]	
	private static extern void SetPosition(float[] position);
	[DllImport(DLL_NAME)]	
	private static extern void SetRotation(float[] rotation);
	#endregion

	#region //Import plugin Getters
	[DllImport(DLL_NAME)]
	private static extern float getX();
	[DllImport(DLL_NAME)]
	private static extern float getY();
	[DllImport(DLL_NAME)]
	private static extern float getZ();

	[DllImport(DLL_NAME)]
	private static extern float getRotX();
	[DllImport(DLL_NAME)]
	private static extern float getRotY();
	[DllImport(DLL_NAME)]
	private static extern float getRotZ();
	#endregion

	

	private void Start()
	{
		#region //resets file values, loads values, and  spawns initial cube

		//sets file vlaues to initial values 
		float[] position = { 0, 2, 0 };
		float[] rotation = { 0, 0, 0 };
		SetPosition(position);
		SetRotation(rotation);
		Save(filePath);

		//takes newly generated file values and spawns initial cube
		Load(filePath);
		cube = Instantiate(LoadableCube, new Vector3(getX(), getY(), getZ()), Quaternion.Euler(getRotX(), getRotY(), getRotZ()));
		#endregion
	}

	void Update()
	{
		#region //Save Button
		//Checks for keypresses
		if (Input.GetKeyDown(KeyCode.K))
		{
			//gets existing cube position as a vector
			Vector3 pos = cube.transform.position;
			//takes vector coordinate and saves as array of floats
			float[] position = { pos.x, pos.y, pos.z };
			//gets existing cube rotation as a vector
			Vector3 rot = cube.transform.rotation.eulerAngles;
			//takes vector values and saves as array of floats
			float[] rotation = { rot.x, rot.y, rot.z };

			//uses array of floats to set position via plugin
			SetPosition(position);
			//uses array of floats to set rotation via plugin
			SetRotation(rotation);

			//Saves the position and rotation set above to the file via plugin
			Save(filePath);
		}
		#endregion

		#region //Load Button
		//checks for keypresses
		if (Input.GetKeyDown(KeyCode.L))
		{
			//destroys the current instance of the cube
			DestroyImmediate(cube);
			//safety
			cube = null;

			//loads position and rotation data from file via plugin
			Load(filePath);


			//Plugin gets 
			cube = Instantiate(LoadableCube, new Vector3(getX(), getY(), getZ()), Quaternion.Euler(getRotX(), getRotY(), getRotZ()));

			
			
		}
		#endregion
	}
}